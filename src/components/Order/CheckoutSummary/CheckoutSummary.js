import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import classes from './CheckoutSummary.css'

const checkoutSummary = (props) => {
    return(
        <div className={classes.CheckoutSummary}>
            <h1>hope it will be good!</h1>
            <div style={{width: '100%', margin: 'auto'}}>
                <Burger ingredients={props.ingredients}/>
            </div>
            <Button btnType='Danger' clicked={props.onCheckoutCancelled}>Cancel</Button>
            <Button btnType='Success' clicked={props.onCheckoutConfirmation}>Confirm</Button>
        </div>
    )
};

export default checkoutSummary;