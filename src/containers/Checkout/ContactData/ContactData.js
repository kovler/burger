import React, { Component } from 'react';

import Button from '../../../components/UI/Button/Button';
import axios from '../../../axios-order';
import Spinner from '../../../components/UI/Spinner/Spinner';

import classes from './ContactData.css';



class ContactData extends Component{

    state = {
        name: '',
        email: '',
        address:{
            street: '',
            postalCode: ''
        },
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        });
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {
                name: 'ariel',
                address: 'lilinbloom 13',
                email: 'arielkovler@gmail.com'
            },
            deliveryMethod: 'fastest'
        };
        axios.post('/order.json', order).then(response => {
            this.setState({
                loading: false
            });
            this.props.history.push('/')
        }).catch(error => {
            this.setState({
                loading: false
            });
        });
    };

    render(){
        let form = (
            <form>
                <input className={classes.Input} type='text' name='name' placeholder='your name'/>
                <input className={classes.Input} type='text' name='email' placeholder='email'/>
                <input className={classes.Input} type='text' name='street' placeholder='street address'/>
                <input className={classes.Input} type='text' name='postal' placeholder='postal code'/>
                <Button btnType='Success' clicked={this.orderHandler}>Confirm</Button>
            </form>
        );
        if(this.state.loading){
            form = <Spinner/>
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter your data here</h4>
                {form}
            </div>
        )
    }
}

export default ContactData;