import React from 'react';
import classes from './BuildControl.css';

const buildConrol = (props) => (
    <div className={classes.BuildControl}>
        <div className={classes.Label}>{props.label}</div>
        <button onClick={props.add} className={classes.More}> More </button>
        <button onClick={props.remove} disabled={props.disable} className={classes.Less}> Less </button>
    </div>
);

export default buildConrol;