import React, {Component} from 'react';
import axios from '../../axios-order';

import Aux from '../../hoc/Auxi';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrdarSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/WithErrorHandler/WithErrorHandler';

const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

class BurgerBuilder extends Component {
    state = {
        ingredients: null,
        price: 4,
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false
    };

    componentDidMount() {
        axios.get('https://burger-68a62.firebaseio.com/ingredients.json').then(res => {
            this.setState({
                ingredients: res.data
            })
        }).catch(error => {
            this.setState({
                error: true
            })
        })
    }

    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients).map(igKey => {
            return ingredients[igKey]
        }).reduce((sum, el) => {
            return sum + el;
        }, 0);
        this.setState({
            purchasable: sum > 0
        });
    };

    addIngredient = (type) => {
        const current_ing = this.state.ingredients[type];
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = current_ing + 1;
        const current_price = this.state.price;
        const new_price = current_price + INGREDIENT_PRICES[type];
        this.setState({
            price: new_price,
            ingredients: updatedIngredients
        });
        this.updatePurchaseState(updatedIngredients);
    };

    removeIngredient = (type) => {
        const current_ing = this.state.ingredients[type];
        if (current_ing <= 0){
            return;
        }
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = current_ing - 1;
        const current_price = this.state.price;
        const new_price = current_price - INGREDIENT_PRICES[type];
        this.setState({
            price: new_price,
            ingredients: updatedIngredients
        });
        this.updatePurchaseState(updatedIngredients);
    };

    purchasingHandler = () => {
        this.setState({purchasing: true})
    };

    purchasingCancelHandler = () => {
        this.setState({purchasing: false})
    };

    purchaseContinueHandler = () => {
        const queryList = [];
        for(let i in this.state.ingredients){
            queryList.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]))
        }
        queryList.push('price=' + this.state.price);
        const queryString = queryList.join('&');
        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString
        });

    };

    render() {
        const disableInfo = {
            ...this.state.ingredients
        };
        for(let key in disableInfo){
            disableInfo[key] = disableInfo[key] <= 0;
        }



        let orderSummary = null;
        let burger = this.state.error ? <p>ingredients cant be loaded</p> : <Spinner/>;
        if(this.state.ingredients){
            burger = (
                <Aux>
                    <Burger ingredients={this.state.ingredients}/>
                    <BuildControls
                        ingredients={Object.keys(this.state.ingredients)}
                        addFunc={this.addIngredient}
                        removeFunc={this.removeIngredient}
                        disabled={disableInfo}
                        price={this.state.price}
                        purchasable={this.state.purchasable}
                        order={this.purchasingHandler}
                    />
                </Aux>
            );
            orderSummary = <OrderSummary
                purchasedCancelled={this.purchasingCancelHandler}
                purchasedContinued={this.purchaseContinueHandler}
                ingredients={this.state.ingredients}
                price={this.state.price}
            />;
        }
        if(this.state.loading){
            orderSummary = <Spinner/>
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchasingCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

export default withErrorHandler(BurgerBuilder, axios);