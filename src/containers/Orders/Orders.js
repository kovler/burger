import React, { Component } from 'react';

import Order from '../../components/Order/Order';
import axios from '../../axios-order';


class Orders extends Component{

    state = {
        orders: [],
        loading: true
    };

    componentDidMount() {
        axios.get('/order.json').then(response => {
            const fetchedOrders = [];
            for(let key in response.data){
                fetchedOrders.push({
                    ...response.data[key],
                    id: key
                });
            }
            this.setState({
                loading: false,
                orders: fetchedOrders
            })
        }).catch(e => {
            this.setState({
                loading: false
            })
        })
    }

    render (){
        return(
            <div>
                {this.state.orders.map( order => (
                    <Order
                        key={order.id}
                        ingredients={order.ingredients}
                        price={+order.price}
                        customer={order.customer}
                        method={order.method}
                    />
                ))}
            </div>
        )
    }
}

export default Orders