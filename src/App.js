import React, {Component} from 'react';
import './App.css';
import Layout from './containers/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
import Orders from './containers/Orders/Orders';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

class App extends Component {


    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Layout>
                        <Switch>
                            <Route path='/checkout' component={Checkout}/>
                            <Route path='/Orders' component={Orders}/>
                            <Route path='/' exact component={BurgerBuilder}/>
                        </Switch>
                    </Layout>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
