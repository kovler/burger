import React from 'react';

import classes from './Logo.css';
import burgerLogo from '../../assets/images/logo.png';

const logo = (props) => (
    <div className={classes.Logo} style={{height: props.height, marginBottom: props.marginB}}>
        <img onClick={props.clicked} src={burgerLogo}/>
    </div>
);


export default logo;