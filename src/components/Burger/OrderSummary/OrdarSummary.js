import React from 'react';

import Aux from '../../../hoc/Auxi'
import Button from "../../UI/Button/Button";

const orderSammary = (props) => {

    const orderSummary = Object.keys(props.ingredients).map(igKey => {
        return (
            <li key={igKey}>
                <span style={{textTransform: 'capitalize'}}>{igKey}</span>
                : {props.ingredients[igKey]}
            </li>
        )
    });

    return (
        <Aux>
            <h3>your summary</h3>
            <ul>
                {orderSummary}
            </ul>
            <p>The total price is: {props.price.toFixed(2)}</p>
            <p>would you like to checkout?</p>
            <Button clicked={props.purchasedCancelled} btnType='Danger'>CANCEL</Button>
            <Button clicked={props.purchasedContinued} btnType='Success'>CONTINUE</Button>
        </Aux>
    )

};

export default orderSammary;