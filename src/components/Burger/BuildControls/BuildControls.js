import React from 'react';
import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const buildControls = (props) => {

    const ingredientsList = props.ingredients.map(el => {
        return (

            <BuildControl
                label={el}
                key={el}
                add={() => props.addFunc(el)}
                remove={() => props.removeFunc(el)}
                disable={props.disabled[el]}
            />

        )
    });

    return (
        <div className={classes.BuildControls}>
            <p>current price: {props.price.toFixed(2)}</p>
            {ingredientsList}
            <button
                onClick={props.order}
                disabled={!props.purchasable}
                className={classes.OrderButton}>order now
            </button>
        </div>
    );
};


export default buildControls;